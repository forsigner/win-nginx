## win-nginx
nginx for window

---------------------------------

#### 用法：

1. 进入任意目录，比如：<code>cd c:\</code>
2. 然后下载nginx，<code>git clone git@github.com:forsigner/win-nginx.git</code>
3. 进入nginx目录，<code>cd win-nginx</code>
4. 在此目录执行各种nginx命令，比如开启、关闭、重启等

#### 常用命令：

``` python
start nginx # 启动nginx
nginx -s stop # 停止配置文件
nginx -t # 检查配置文件是否争取
nginx -s reload # 重新加载配置文件
```
